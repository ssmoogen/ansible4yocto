# That Yother Thing (TYT) Setup

TYT is a set of enviroments to setup and build out small environments
for working on various projects. Originally this was aimed at Yocto but
has been growing to add in osbuild, mock, and potentially many other
tools. 

## Layout

Layout of the ansible tree is based off the Fedora and CentOS ansible
trees. 

Files which need editing for others to use:
* ansible.cfg
* inventory/inventory
* inventory/group_vars/all
* inventory/host_vars/(name of hosts)

Directories known to not be tracked

Currently the gitignore tries not to track directories named
private. 

**FIXME: make these vault directories**

## Targeted Architectures

This project will try to make sure it works on
* aarch64 (primary)
* x86_64 (secondary)

## Targets Operating Systems

This project will test to make sure it works on
* RHEL 8.4 (primary)
* CentOS Stream 8 (secondary)

## Test Infrastructure

**FIXME: needs a testing system beyond my home boxes**
