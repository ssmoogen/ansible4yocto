# Playbook Naming Convention #

## Adhoc

General playbooks which are meant to run once on building or to set a
particular setting should be called adhoc-(class).yml 

## Routine

Playbooks which are to be run regularly on systems should have a name
starting with routine-(class).yml
