# Groups this set of playbooks use

```
[yocto-raspi4]
[yocto:children]
<blah>

[osbuild_centos8]
<blah>

[osbuild_rhel8]
<blah>

[osbuild:children]
osbuild_centos8
osbuild_rhel8

[kvm_server]
<blah>

[mockbuilder_c8s_aarch64]
<blah>

[mockbuilder_c8s_x86_64]
<blah>

[mockbuilder_r8_x86_64]
<blah>

[mockbuilder:children]
mockbuilder_c8s_aarch64
mockbuilder_c8s_x86_64
mockbuilder_r8_x86_64



```
